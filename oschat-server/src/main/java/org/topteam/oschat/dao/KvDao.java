package org.topteam.oschat.dao;

import java.util.Map;

public interface KvDao {
	
	KvDao open(String db);
	
	void close();

	String get(String key);
	
	Map<String,String> query(String prefix);

	void put(String key, String value);
	
	void putObj(String key, Object value);

	void put(Map<String, String> objects);
	
	<T> void putObj(Map<String, T> objects);
	
	void remove(String key);
	
	void removeAll();
	
	void batch(Batch batch);
	
	long size();
	
	String stats();
}
