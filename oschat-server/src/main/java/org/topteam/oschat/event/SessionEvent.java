package org.topteam.oschat.event;

import java.io.Serializable;

import javax.websocket.Session;

import org.topteam.oschat.entity.User;

/**
 * WebSocket Session相关事件
 * @see javax.websocket.Session
 * @author JiangFeng
 *
 */
public class SessionEvent {

	/**
	 * 打开WebSocket 连接
	 * @author JiangFeng
	 *
	 */
	public static class SessionOpen implements Serializable {

		private static final long serialVersionUID = -7600347876267975997L;

		private Session session;

		private User user;

		private String withId;

		public SessionOpen(User user, String withId, Session session) {
			this.user = user;
			this.withId = withId;
			this.session = session;
		}

		public Session getSession() {
			return session;
		}

		public void setSession(Session session) {
			this.session = session;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public String getWithId() {
			return withId;
		}

		public void setWithId(String withId) {
			this.withId = withId;
		}

	}

	/**
	 * 关闭Websocket连接
	 * @author JiangFeng
	 *
	 */
	public static class SessionClose implements Serializable {

		private static final long serialVersionUID = -3701789725873868399L;
		private Session session;
		
		private String withId;
		
		private String userId;

		public SessionClose(String userId,String withId,Session session) {
			this.session = session;
			this.withId =withId;
			this.userId = userId;
		}

		public Session getSession() {
			return session;
		}

		public void setSession(Session session) {
			this.session = session;
		}

		public String getWithId() {
			return withId;
		}

		public void setWithId(String withId) {
			this.withId = withId;
		}

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

	}
	
	/**
	 * 丢失WebSocket主连接
	 * @author JiangFeng
	 *
	 */
	public static class LostConnection implements Serializable{

		private static final long serialVersionUID = -352589589432753460L;
		
	}

}
