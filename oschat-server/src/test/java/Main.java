import org.topteam.oschat.dao.KvDao;
import org.topteam.oschat.event.UserEvent;
import org.topteam.oschat.message.Data;

import com.alibaba.fastjson.JSON;


public class Main {

	public static void main(String[] args) {
		Data message = Data.buildTextMessage("aaa", "1", "2");
		
		Data e = Data.buildEventMessage(new UserEvent.GetFirends());
		System.out.println(JSON.toJSONString(e));
		
		Data d = Data.buildTextMessage("测试", "1", "2");
		System.out.println(JSON.toJSONString(d));
	}
}
