/**
 * Created by clarie on 14-7-18.
 */
(function(){

    oschat.WindowSize=WindowSize;
    var EXTEND = null;//是否需要继承
    function WindowSize(){
        if(EXTEND){
            EXTEND.apply(this, arguments);
        }
        this._sizeFlag=true;
        this._gui=require('nw.gui');
        this._win=this._gui.Window.get();
        this.init();
        var self=this;
        $('#minisizeBtn').bind('click',function(){
            self.minimize();
        });
        $('#plusBtn').bind('click',function(){
            self.maxToggle();
        });
        $('#closeBtn').bind('click',function(){
            self.close();
        });

    }
    WindowSize.prototype={
        constructor:WindowSize,
        init:function(){

        },
        minimize:function(){
            this._win.minimize();
            this._sizeFlag = true;
        },
        maxToggle:function(){
            if(this._sizeFlag){
                this._win.maximize();
                this._sizeFlag = false;
            }
            else{
                this._win.unmaximize();
                this._sizeFlag = true;
            }
        },
        close:function(){
            this._win.close();
        }




    }
})();

(function(){
    oschat.DefaultSetting=DefaultSetting;
    var EXTEND = null;//是否需要继承
    function DefaultSetting(){
        if(EXTEND){
            EXTEND.apply(this, arguments);
        }
        this._setbtn=$('#setBtn');
        this._windowsize=new oschat.WindowSize();
        var self=this;
        this._setbtn.click(function(){
            self._windowsize._gui.Window.open(
                'settings.html',{
                    "title": "zoo(专业版)",
                    "icon": "logo.png",
                    "toolbar": false,
                    "frame": false,
                    "max_width": 568,
                    "max_height": 459,
                    "width": 568,
                    "height": 459,
                    "position": "right",
                    "min_width": 568,
                    "min_height": 459,
                    "resizable":true
                }
            );
        });
    }


})();

