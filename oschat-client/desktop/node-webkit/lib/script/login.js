/**
 * Created by clarie on 14-7-23.
 */
(function () {
    oschat.Login = Login;
    var EXTEND = null;

    function Login() {
        if (EXTEND) {
            EXTEND.apply(this, arguments);
        }
        this._gui = require('nw.gui');
        this._win = this._gui.Window.get();
        this._loginbtn = $('#loginBtn');
        this._cog = $('#cog');
        this._minus = $('#minus');
        this._remove = $('#remove');
        var self = this;
        this._loginbtn.click(function () {
            self._gui.Window.open(
                'index.html', {
                    "title": "zoo(专业版)",
                    "icon": "logo.png",
                    "toolbar": true,
                    "frame": false,
                    "width": 300,
                    "height": 600,
                    "position": "center",
                    "min_width": 300,
                    "min_height": 600,
                    "resizable": true
                });
            self._win.close();
        });

        this._cog.click(function () {
            $('#loginPage').hide();
            $('body')[0].style.background = 'url(imgs/set.png) no-repeat'
            $('#loginSet').show();
        });

        this._minus.click(function () {
            self._win.minimize();
        });
        this._remove.click(function () {
            self._win.close();
        });

    }

})();
