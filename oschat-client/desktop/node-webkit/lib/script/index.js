/**
 * Created by clarie on 14-7-21.
 */
(function(){
    oschat.Index=Index;
    var EXTEND = null;//是否需要继承
    function Index(){
        if(EXTEND){
            EXTEND.apply(this, arguments);
        }
        this.init();
    }

    Index.prototype={
        constructor:Index,
        init:function(){
            this._mesBoard=new oschat.MesBoard();
            this._search=new oschat.Search();
            this._content=new oschat.Content();
            this._stat=new oschat.Stat();
            this._mesBoard.setWidth();
            this._search.setWidth();
            this._content.getHeight();
            var self=this;
            this._content._windowSize._win.on('resize',function(){
                self._mesBoard.setWidth();
                self._search.setWidth();
                self._content.getHeight();
            });
        }
    }
})();

(function(){
    oschat.MesBoard=MesBoard;
    var EXTEND = null;//是否需要继承
    function MesBoard(){
        if(EXTEND){
            EXTEND.apply(this, arguments);
        }
        this.init();
        this._messageBoard = $('#messageBoard');

    }

    MesBoard.prototype={
        constructor:MesBoard,
        init:function(){

        },
        setWidth:function(){
            this._user=$('#user');
            console.log(this._user.width());
            this._messageBoard.width(this._user.width()-86+'px');
        },
        click:function(){
            this.select();
            var self=this;
            self._messageBoard.one('blur', function() {
                var newvalue=self._messageBoard.val();
                self.save(newvalue);
//            }
            });
        },
        save:function(value){
            console.info(value);
        }
    }


})();

(function(){
    oschat.Search=Search;
    var EXTEND = null;//是否需要继承
    function Search(){
        if(EXTEND){
            EXTEND.apply(this, arguments);
        }
        this.init();
    }

    Search.prototype={
        constructor:Search,
        init:function(){

        },
        setWidth:function(){
            this._search=$('#search');
            this._search.width(this._search.parent().width()-14+'px');
        }
    }
})();
